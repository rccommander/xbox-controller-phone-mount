## Rework of jerwil's work on Thingiverse
Xbox One S Controller Phone Mount with Modular Mounting System by jerwil on Thingiverse: https://www.thingiverse.com/thing:2553522

## Images
<img src="images/IMG_1_rework.jpg" alt="front" height="300"/>
<img src="images/IMG_2_rework.jpg" alt="side" height="300"/>
<img src="images/IMG_3_rework.jpg" alt="back" height="300"/>
<img src="images/IMG_4_rework.jpg" alt="back_with_phone" height="300"/>

## Changes
- reworked Xbox_One_Clip_with_Modular_Mounting_System.stl to release interfaces on the back of the Xbox controller
- added FreeCAD (0.19) and .obj file of the reworked stl file
- renamed LICENSE.txt to LICENSE
- renamed README.txt to README.md
- added link to license in LICENSE
- added images of the reworked and assembled mount in /images
- added heading in README
- added images in README
- added changes in README
- revised folder structure
